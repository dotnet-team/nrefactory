Source: nrefactory
Section: cli-mono
Priority: optional
Maintainer: Debian CLI Libraries Team <pkg-cli-libs-team@lists.alioth.debian.org>
Uploaders: Jo Shields <directhex@apebox.org>
Build-Depends: debhelper (>= 7.0.50),
 cli-common-dev (>= 0.8~),
 mono-devel (>= 2.10~),
 libgtk2.0-cil-dev,
 libmono-cecil-cil-dev (>= 0.9.5~)
Standards-Version: 3.9.4
Homepage: https://github.com/icsharpcode/NRefactory
Vcs-Browser: http://git.debian.org/?p=pkg-cli-libs/packages/nrefactory.git
Vcs-Git: git://git.debian.org/git/pkg-cli-libs/packages/nrefactory.git

Package: libicsharpcode-nrefactory-cil-dev
Depends: libicsharpcode-nrefactory5.0-cil (= ${binary:Version}),
         libicsharpcode-nrefactory-cecil5.0-cil (= ${binary:Version}),
         libicsharpcode-nrefactory-csharp5.0-cil (= ${binary:Version}),
         libicsharpcode-nrefactory-ikvm5.0-cil (= ${binary:Version}),
         libicsharpcode-nrefactory-xml5.0-cil (= ${binary:Version}),
         ${misc:Depends}
Architecture: all
Description: C# Parsing and Refactoring Library - Development files
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains development files for compiling against to use
 the NRefactory library.

Package: libicsharpcode-nrefactory5.0-cil
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends},
         libicsharpcode-nrefactory-cecil5.0-cil
Suggests:
         libicsharpcode-nrefactory-ikvm5.0-cil
Architecture: all
Description: C# Parsing and Refactoring Library - Core
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains the core NRefactory library.

Package: libicsharpcode-nrefactory-cecil5.0-cil
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends}
Architecture: all
Description: C# Parsing and Refactoring Library - Cecil back-end
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains the Mono.Cecil back-end for the NRefactory 
 library.

Package: libicsharpcode-nrefactory-csharp5.0-cil
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends}
Architecture: all
Description: C# Parsing and Refactoring Library - C# Component
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains the C# component for the NRefactory library.

Package: libicsharpcode-nrefactory-ikvm5.0-cil
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends}
Architecture: all
Description: C# Parsing and Refactoring Library - IKVM back-end
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains the IKVM back-end for the NRefactory library.

Package: libicsharpcode-nrefactory-xml5.0-cil
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends}
Architecture: all
Description: C# Parsing and Refactoring Library - XML
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains the XML component for the NRefactory library.

Package: nrefactory-samples
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends}
Architecture: all
Description: C# Parsing and Refactoring Library - Sample programs
 NRefactory is the C# analysis library used in the SharpDevelop and 
 MonoDevelop IDEs. It allows applications to easily analyze both syntax 
 and semantics of C# programs. It is quite similar to Microsoft's 
 Roslyn project; except that it is not a full compiler – NRefactory 
 only analyzes C# code, it does not generate IL code.
 .
 This package contains sample programs for the NRefactory library.
